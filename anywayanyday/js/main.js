(function(context){
    'use strict';
    //Вспомогательный класс
    context.HelpClass = function(){
        this._lng = {
            month : [
                'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'
            ]
        };
        //Вспомогательное свойство, для предотвращения запуска нескольких запросов одновременно
        this._startRequest = false;
    };
    //Общий метод для запроса к серверу
    context.HelpClass.prototype._Request = function(option){
        if(this._startRequest) return;
        this._startRequest = true;
        option.data._Serialize = 'JSON';
        var self = this;
        $.ajax({
            url: option.url,
            type: 'GET',
            dataType: 'jsonp',
            data: option.data
        }).always(function() {
            self._startRequest = false;
        }).done(function(data) {
            if(data.Error){console.log(data.Error);}
            option.callback(data);
        });
    };
    //Формирование даты
    context.HelpClass.prototype._formatDate = function(time){
        if(!time) return null;
        var date = time.substr(0,2),
            month = this._lng.month[(+time.substr(2,2))-1],
            hour = time.substr(4,2),
            minutes = time.substr(6,2);
        return date + ' ' + month + ', ' + (+hour) + ':' + minutes;
    };
    //Формирование времени
    context.HelpClass.prototype._formatTime = function(time){
        if(!time) return null;
        return (+time.substr(0,2)) + 'ч ' + time.substr(2,2) + 'мин';
    };

    //Класс поиска
    context.Search = function(){
        context.HelpClass.call(this);
        //Конфигурации класса
        this._config = {
            //Набор ссылок апи
            api : {
                start: 'http://api.anywayanyday.com/api/NewRequest3/',
                status: 'http://api.anywayanyday.com/api/RequestState/',
                result: 'http://api.anywayanyday.com/api/Fares2/'
            },
            //Время обновления информации в минутах
            timeUpdateInfo: 1,
            //Время обновления статуса в милисекундах
            timeUpdateStatus: 2000
        };
        this._tickets = {};
        this._Reference = {};
        this._blocks = {};
        this._idSynonym = '';
        this.idRequest = '2208MOWPARAD1CN0IN0SCE';
        this._filterCompany = '';
    };
    context.Search.prototype = Object.create(context.HelpClass.prototype);
    context.Search.cronstructor = context.Search;

    //Защищенные методы(условно конечно)
    //Перебираем вспомогательную инормацию и загоняем в объект,
    //чтобы потом по ключам удобнее и быстрее вытаскивать
    context.Search.prototype._setReferences = function(data){
        for(var name in data){
            this._setReference(data[name], name);
        }
    };
    context.Search.prototype._setReference = function(data, name){
        for(var i = data.length - 1; i >= 0; i--){
            if(!this._Reference[name]){
                this._Reference[name] = {};
            }
            this._Reference[name][data[i].Code] = data[i]
        }
    };
    //Запуск поиска
    context.Search.prototype._startSearch = function(){
        var self = this;
        if(self.searchStarted) return;
        self.searchStarted = true;
        var callback = function(data){
            self._idSynonym = data.IdSynonym;
            self._blocks.buttonText.html('Начинаем искать рейсы');
            self._checkStatus();
        };
        this._Request({
            data: {
                'Route' : this.idRequest
            },
            url: this._config.api.start,
            callback: callback
        });
    };
    //Проверка статуса поиска
    context.Search.prototype._checkStatus = function(){
        var self = this;
        var callback = function(data){
            var status = data.Completed;
            self._blocks.loader.css('width', status+'%');

            if(status === "100"){
                self._loadResult();
                return;
            }
            self._blocks.buttonText.html('Собираем информацию.<br> Готово на ' + status + '%');

            setTimeout(function(){
                self._checkStatus.call(self);
            }, self._config.timeUpdateStatus);
        };
        this._Request({
            data : {
                'R' : this._idSynonym
            },
            url : this._config.api.status,
            callback: callback
        });

    };

    //Обработка полученной информации по билетам
    //Перебор авиокомпаний
    context.Search.prototype._separateAirlines = function(data){
        for(var i = data.length - 1; i >=0; i--){
            var companyName = this._Reference.Carriers[data[i].Code].Name;
            this._separateTickets(data[i].Fares, companyName);
        }
    };
    //Перебор билетов
    context.Search.prototype._separateTickets = function(tickets, companyName){
        for(var i = tickets.length - 1; i >=0; i--){
            this._setTicket(tickets[i],companyName)
        }
    };
    //Запись билетов
    context.Search.prototype._setTicket = function(ticket, companyName){
        if(this._tickets[ticket.FareId]) return;
        var thisTicket = {};
        thisTicket.price = ticket.TotalAmount;
        thisTicket.bonus = ticket.BonusLevel[0] ? ticket.BonusLevel[0].Amount : 0;
        thisTicket.isLowCost = ticket.IsLowCost;
        thisTicket.company = companyName;

        var thisDirs = [];
        for(var i = 0, j = ticket.Directions.length ; i < j; i++){
            thisDirs[i] = this._forDirection(ticket.Directions[i]);
        }
        thisTicket.direction = thisDirs;
        this._tickets[ticket.FareId] = thisTicket;
    };
    //Перебор направлений
    context.Search.prototype._forDirection = function(direction){
        var thisDir = [];
        thisDir.haveChange = direction.Points.length > 2;
        for(var i = 0, j = direction.Variants.length; i < j; i++){
            var thisVar = {};
            thisVar.totalTime = this._formatTime(direction.Variants[i].TravelTime)
            thisVar.legs = this._forLegs(direction.Variants[i].Legs, direction.Points);
            thisDir.push(thisVar);
        }
        return thisDir;
    };
    //Перебор точек перелета
    context.Search.prototype._forLegs = function(legs, points){
        var thisLegs = [];
        for(var i = 0, j = legs.length ; i < j; i++){
            var leg = {};
            leg.arrivalDate = this._formatDate(legs[i].ArrivalDate);
            leg.departureDate = this._formatDate(legs[i].DepartureDate);
            leg.durationOfChange = this._formatTime(legs[i].DurationOfChange);
            leg.class = legs[i].Class;
            leg.flightDuration = this._formatTime(legs[i].FlightDuration);
            leg.flightNumber = legs[i].FlightNumber;
            leg.plane = this._Reference.Planes[legs[i].Plane].Name;
            leg.departurePlace = this._getPlace(points[i], true);
            leg.arrivalPlace = this._getPlace(points[i+1], false);
            thisLegs.push(leg);
        }
        return thisLegs;
    };
    //Выбор название аэропорта
    context.Search.prototype._getPlace = function(point, departure){
        if(!point) return;
        var code = departure ? point.DepartureCode || point.ArrivalCode : point.ArrivalCode || point.DepartureCode;
        var terminal = departure ? point.DepartureTerminal || point.DepartureTerminal : point.DepartureTerminal || point.DepartureTerminal;
        var airport = this._Reference.Airports[code];
        var airportStr = airport.Country + ', '+airport.City+', '+airport.Name;
        if(terminal) airportStr += ' ' + terminal;
        return airportStr;
    };

    //Рендер билета
    context.Search.prototype._renderTicket = function(ticket){
        var renderedTicket = '<article class="ticket">';
        renderedTicket += '<div class="ticket__header">';
        renderedTicket += '<p class="ticket__company">Компания: '+ticket.company+'</p>';
        if(ticket.bonus)
            renderedTicket += '<p class="ticket__bonus">Бонус:'+ticket.bonus+'</p>';
        if(ticket.isLowCost)
            renderedTicket += '<p class="ticket__low-cost">Это дешевая авиалиния</p>';
        renderedTicket += '<p class="ticket__price">'+ticket.price+' руб.</p>';
        renderedTicket += '</div>';

        for(var i = 0, j = ticket.direction.length; i < j; i++){
            if(i === 1) renderedTicket += '<p class="variant_back">Есть билеты обратно:</p>';
            renderedTicket += this._renderVariants(ticket.direction[i]);
        }
        renderedTicket += '</article>';
        return renderedTicket;
    };
    //Рендер вариантов
    context.Search.prototype._renderVariants = function(variants){
        var renderedVariants = '';
        for(var i = 0, j = variants.length; i < j; i++){
            renderedVariants += '<div class="variant">';
            renderedVariants += '<p>Всего в пути: '+variants[i].totalTime+'</p>';
            renderedVariants += this._renderLegs(variants[i].legs);
            renderedVariants += '</div>';
        }
        return renderedVariants;
    };
    //Рендер точек перелета
    context.Search.prototype._renderLegs = function(legs){
        var renderedLegs = '';
        for(var i = 0, j = legs.length; i < j; i++){
            renderedLegs += '<p>Вылет из: '+legs[i].departurePlace+'</p>';
            renderedLegs += '<p>Дата вылета: '+legs[i].departureDate+'</p>';

            renderedLegs += '<p>Прибытие в: '+legs[i].arrivalPlace+'</p>';
            renderedLegs += '<p>Дата прибытия: '+legs[i].arrivalDate+'</p>';

            renderedLegs += '<p>Название самолета: '+legs[i].plane+'</p>';
            renderedLegs += '<p>Номер рейса: '+legs[i].flightNumber+'</p>';
            renderedLegs += '<p>Продолжительность полета: '+legs[i].flightDuration+':</p>';
            renderedLegs += '<p>Класс: '+legs[i].class+':</p>';
            if(legs[i].durationOfChange) {
                renderedLegs += '<p>Пересадка (' + legs[i].durationOfChange + '):</p>';
            }
        }
        return renderedLegs;
    };
    //Вывод билетов
    context.Search.prototype._getTickets = function(){
        var tickets = [];
        for(var i in this._tickets){
            if(this._filterCompany && this._filterCompany !== this._tickets[i].company) continue;
            tickets.push(this._tickets[i]);
        }
        tickets.sort(function(a,b){
            return  a.price -b.price;
        });
        var renderedTicket = '';
        for(var i = 0, j = tickets.length; i < j; i++){
            renderedTicket += this._renderTicket(tickets[i]);
        }
        this._blocks.tickets.html(renderedTicket);
    };
    //Вывод списка компаний
    context.Search.prototype._getCompany = function(){
        var companies = [];
        for(var i in this._Reference.Carriers){
            companies.push(this._Reference.Carriers[i].Name);
        }
        companies.sort();
        var renderCompany = '';
        for(var i = 0, j = companies.length; i < j; i++){
            renderCompany += '<li><button class="choose-company__button">'+companies[i]+'</button></li>';
        }
        this._blocks.companyList.html(renderCompany);
    };
    //Загрузка результата
    context.Search.prototype._loadResult =  function() {
        var self = this;
        self._blocks.buttonText.html('Готово! Загружаем результат...');
        var callback = function (data) {
            self.searchStarted = false;
            //Запоминаем данные по ключам, чтобы потом не перебирать их заново
            self._setReferences(data.References);
            self._separateAirlines(data.Airlines);
            self._getTickets();
            self._getCompany();
            self._blocks.buttonStart.fadeOut(500, function(){
                self._blocks.result.fadeIn(500);
            });
            setTimeout(function(){
                self._startSearch.call(self);
            }, self._config.timeUpdateInfo * 60000);

        };
        this._Request({
            data: {
                'R': this._idSynonym,
                'L': 'RU',
                'C': 'RUB',
                'Limit': 200,
                'DebugFullNames': true
            },
            url: this._config.api.result,
            callback: callback
        });
    };

    //Публичные методы
    ////Установка параметров
    context.Search.prototype.init = function(data){
        this._blocks = {
            buttonStart : $(data.buttonStart),
            loader : $(data.loader),
            buttonText : $(data.buttonText),
            tickets : $(data.tickets),
            companyList : $(data.companyList),
            companyButton : $(data.companyButton),
            result : $(data.result)
        };
        var self = this;
        this._blocks.buttonStart.click(function(){
            self._startSearch();
        });
        $(document).on('click', data.companyButton, function(){
            self._filterCompany = $(this).text();
            self._getTickets();
        });

    }
}(window));
search = new Search();
search.init({
    buttonStart : '.button-start',
    loader: '.button-start__loader',
    buttonText : '.button-start__text',
    tickets : '.tickets',
    companyList : '.choose-company',
    companyButton : '.choose-company__button',
    result : '.result'
});